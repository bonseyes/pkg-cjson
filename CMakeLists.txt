# CJSON
if(NOT PKG_CJSON_ENABLE)
  return()
endif()

if (CMAKE_VERBOSE)
  message("pkg-cjson enabled")
endif()

set(CJSON_LIBRARIES cjson CACHE STRING "List of cJSON libraries to be linked with" FORCE)

set(CJSON_INCLUDE_DIRS ${CMAKE_BINARY_DIR}/deps/include CACHE PATH "cJSON include directory" FORCE)

set(cjson_args
    -DCMAKE_INSTALL_PREFIX=${CMAKE_BINARY_DIR}/deps
    -DBUILD_SHARED_LIBS=OFF
    -DENABLE_CJSON_TEST=OFF
)

if(IOS)
    set(cjson_args ${cjson_args} -DCMAKE_C_COMPILER_FORCED=TRUE)
endif()

add_cmake_project(cjson_project "src" "${cjson_args}")
